﻿using CombatSystem.Scripts.Player;
using UnityEngine;

namespace CombatSystem.Scripts.Enemy
{
    public class EnemyController : MonoBehaviour
    {
        #region vars

        [Header("Combat Settings")] 
        [SerializeField] private float maxHealth;
        [SerializeField] private float damage;

        private float currentHealth;

        #endregion

        #region behaviours

        private void OnCollisionEnter2D(Collision2D col)
        {
            var player = col.collider.GetComponent<PlayerController>();

            //Trap programming
            if (!player)
                return;
            
            player.TakeDamage(damage);
        }

        #endregion
        
        #region combat

        public void TakeDamage(float amount)
        {
            currentHealth -= amount;
            
            if(currentHealth <= 0)
                print("Player Dead!");
        }

        public void Heal(float amount)
        {
            currentHealth += amount;

            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        }

        #endregion
    }
}