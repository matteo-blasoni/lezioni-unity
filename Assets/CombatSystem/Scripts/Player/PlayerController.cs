using UnityEngine;

namespace CombatSystem.Scripts.Player
{
    public class PlayerController : MonoBehaviour
    {
        #region vars

        [Header("Movement Settings")]
        [SerializeField] private float speed = 5f;
        [SerializeField] private float acceleration = 50f;

        [Header("Combat Settings")] 
        [SerializeField] private float maxHealth = 100f;

        private float currentHealth;
        
        private Rigidbody2D rb;

        #endregion

        #region behaviours

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
            currentHealth = maxHealth; //Initialize currentHealth
        }

        private void FixedUpdate()
        {
            var hori = Input.GetAxisRaw("Horizontal") * acceleration;
            
            rb.AddForce(new Vector2(hori, 0));

            var clampX = Mathf.Clamp(rb.velocity.x, -speed, speed);

            if (Mathf.Abs(rb.velocity.x) > speed)
                rb.velocity = new Vector2(clampX, rb.velocity.y);
        }

        #endregion

        #region combat

        public void TakeDamage(float amount)
        {
            currentHealth -= amount;
            
            if(currentHealth <= 0)
            {
                print("Player Dead!");
                return;
            }
            
            print($"Player takes {amount} damage.\nCurrent HP: {currentHealth}");
        }

        public void Heal(float amount)
        {
            currentHealth += amount;

            currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        }

        #endregion
    }
}

