﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEditor.SearchService;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace SaveSystem.Scripts
{
    [Serializable]
    public class CoinData
    {
        public int CollectedCoins;
        public List<int> CollectedCoinIds = new List<int>();
    }
    
    public class CoinManager : MonoBehaviour
    {
        [SerializeField] private Text coinsText;

        private CoinData data;
        private string dataPath;

        private void Awake()
        {
            dataPath = $"{Application.persistentDataPath}/Saves";

            Load();
        }

        public void AddCoin(int coinId)
        {
            data.CollectedCoins++; //Change
            data.CollectedCoinIds.Add(coinId);

            coinsText.text = $"Coins: {data.CollectedCoins}";
        }

        public void Save()
        {
            string jsonData = JsonUtility.ToJson(data, true);

            if (!Directory.Exists(dataPath))
                Directory.CreateDirectory(dataPath);

            File.WriteAllText($"{dataPath}/coin_save.data", jsonData);
        }

        public void Load()
        {
            if (!File.Exists($"{dataPath}/coin_save.data"))
            {
                data = new CoinData();
                return;
            }

            string jsonData = File.ReadAllText($"{dataPath}/coin_save.data");
            data = JsonUtility.FromJson<CoinData>(jsonData);
            
            coinsText.text = $"Coins: {data.CollectedCoins}";
        }

        public List<int> GetCoinIds()
        {
            return data.CollectedCoinIds;
        }
    }
}