using SaveSystem.Scripts;
using UnityEngine;

public class CoinCollector : MonoBehaviour
{
    [SerializeField] private CoinManager coinManager;
    
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.TryGetComponent(out CoinId coin))
            return;
        
        coinManager.AddCoin(coin.Id);
        Destroy(col.gameObject);
        
        coinManager.Save();
    }
}
