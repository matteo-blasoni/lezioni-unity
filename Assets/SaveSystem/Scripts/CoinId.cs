﻿using System;
using UnityEngine;

namespace SaveSystem.Scripts
{
    public class CoinId : MonoBehaviour
    {
        [SerializeField] private int id;
        [SerializeField] private CoinManager coinManager;

        public int Id => id;

        private void Start()
        {
            if(coinManager.GetCoinIds().Contains(id))
                Destroy(gameObject);
        }
    }
}