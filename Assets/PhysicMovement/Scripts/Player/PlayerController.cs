using System;
using UnityEngine;

namespace PhysicMovement.Scripts.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerController : MonoBehaviour
    {
        
        #region vars

        [Header("Movement Settings")]
        [SerializeField] private float speed = 5f;
        [SerializeField] private float acceleration = 50f;
        [SerializeField] private float jumpForce = 20f;
        [SerializeField] private float groundDistance = 0.1f;
        [SerializeField] private LayerMask groundMask;

        private Rigidbody2D rb;
        private bool isGrounded;

        #endregion

        #region behaviours

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void Update()
        {
            Jump();
        }

        private void FixedUpdate()
        {
            Move();
            //Jump();
            CheckGround();
        }

        #endregion

        #region movement

        private void Move()
        {
            var hori = Input.GetAxisRaw("Horizontal") * acceleration;

            rb.AddForce(new Vector2(hori, 0)); //F = m * a

            //Limito l'asse interessato dal movimento
            var clampX = Mathf.Clamp(rb.velocity.x, -speed, speed); //Valore di X limitato

            if (Mathf.Abs(rb.velocity.x) > speed)
                rb.velocity = new Vector2(clampX, rb.velocity.y); //X limitata e Y originale del rigidbody
        }

        private void Jump()
        { 
            if(isGrounded && Input.GetButtonDown("Jump"))
                rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
        }

        private void CheckGround()
        {
            var hit = Physics2D.BoxCast(
                transform.position - Vector3.up * (transform.localScale.y / 2f),
                transform.localScale,
                0, Vector2.down, groundDistance, groundMask);

            isGrounded = hit.collider;
        }

        #endregion
    }
}

